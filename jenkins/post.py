import requests
import argparse
import os
import json


def load_post_request(path):
    if not os.path.exists(path):
        raise IOError(2, 'No such file', path)
    with open(path, 'r') as json_data:
        return json.load(json_data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("user", type=str, help="user")
    parser.add_argument("pwd", type=str, help="password")
    parser.add_argument("pr_title", type=str, help="pull request title")
    parser.add_argument("branch_source", type=str, help="branch_source")
    parser.add_argument("branch_target", type=str, help="branch_target")
    parser.add_argument("repo", type=str, help="Repository in which to trigger pull request")
    args = parser.parse_args()

    print("Loading json data")
    json_post = load_post_request(os.path.join(os.getcwd(), 'pullrequest.json'))
    json_post['title'] = args.pr_title
    json_post['source']['branch']['name'] = args.branch_source
    json_post['destination']['branch']['name'] = args.branch_target

    headers = {"Content-Type": "application/json"}
    url = "https://api.bitbucket.org/2.0/repositories/phas006/" + args.repo + "/pullrequests"
    auth = (args.user, args.pwd)
    r = requests.post(url=url, auth=auth, json=json_post, headers=headers)
    print("Response: {}".format(r.text))
    print("Status code: {}".format(r.status_code))