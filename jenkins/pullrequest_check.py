import requests
import argparse
import json
import sys

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("user", type=str, help="user")
    parser.add_argument("pwd", type=str, help="password")
    parser.add_argument("branch_source", type=str, help="branch_source")
    parser.add_argument("branch_target", type=str, help="branch_target")
    parser.add_argument("repo", type=str, help="Repository in which to trigger pull request")
    args = parser.parse_args()

    headers = {"Content-Type": "application/json"}
    url = "https://api.bitbucket.org/2.0/repositories/phas006/" + args.repo + "/pullrequests"
    auth = (args.user, args.pwd)
    response = requests.get(url=url, auth=auth)

    if response.status_code == 200:
        print('Success!')
        json_text = json.loads(response.text)
        for pull_request in json_text['values']:
            if pull_request['destination']['branch']['name'] == args.branch_target and \
               pull_request['source']['branch']['name'] == args.branch_source:
                print('Exists!')
                sys.exit(0)
        else:
            print('Does Not Exist!')
            sys.exit(1)
    elif response.status_code == 404:
        print('Not Found.')
        sys.exit(2)